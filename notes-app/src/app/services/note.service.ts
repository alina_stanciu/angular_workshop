import { Injectable } from '@angular/core';
import { Note } from '../note';

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  notes: Note[] = [
    {
      id: "ID1",
      title: "First note",
      description: "This is the description for the first note",
      categoryId: "1"
    },
    {
      id: "ID2",
      title: "Second note",
      description: "This is the description for the second note",
      categoryId: "2"
    },
    {
      id: "ID3",
      title: "Third note",
      description: "This is the description for the third note",
      categoryId: "3"
    }
  ];

  constructor() { }

  serviceCall() {
    console.log("Note service was called.");
  }

  getNotes() {
    return this.notes;
  }

  getFilteredNotes(argCategoryId: string) {  
    return this.notes.filter(note => note.categoryId === argCategoryId);
  }
}
