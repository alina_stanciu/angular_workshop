import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'notes-app';
  someText: string = "short paragraph";
  someDate: Date = new Date(8, 9, 1999);
  myValue: number = 10;
}
