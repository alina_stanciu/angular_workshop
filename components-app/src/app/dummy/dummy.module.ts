import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DummierModule } from './dummier/dummier.module';
import { FirstDummyComponent } from './dummier/the-dummiest/first-dummy/first-dummy.component';
import { SecondDummyComponent } from './dummier/the-dummiest/second-dummy/second-dummy.component';
import { AnotherDummyComponent } from './dummier/the-dummiest/first-dummy/another-dummy/another-dummy.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DummierModule
  ],
  exports: [
    FirstDummyComponent,
    SecondDummyComponent,
    AnotherDummyComponent
  ]
})
export class DummyModule { }
