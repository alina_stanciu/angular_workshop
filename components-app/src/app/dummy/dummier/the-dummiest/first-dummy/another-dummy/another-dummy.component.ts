import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-another-dummy',
  templateUrl: './another-dummy.component.html',
  styleUrls: ['./another-dummy.component.scss']
})
export class AnotherDummyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
