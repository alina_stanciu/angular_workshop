import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstDummyComponent } from './first-dummy/first-dummy.component';
import { SecondDummyComponent } from './second-dummy/second-dummy.component';
import { AnotherDummyComponent } from './first-dummy/another-dummy/another-dummy.component';



@NgModule({
  declarations: [
    FirstDummyComponent,
    SecondDummyComponent,
    AnotherDummyComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FirstDummyComponent,
    SecondDummyComponent,
    AnotherDummyComponent
  ]
})
export class TheDummiestModule { }
