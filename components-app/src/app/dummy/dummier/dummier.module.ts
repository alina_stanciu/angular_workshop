import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TheDummiestModule } from './the-dummiest/the-dummiest.module';
import { FirstDummyComponent } from './the-dummiest/first-dummy/first-dummy.component';
import { SecondDummyComponent } from './the-dummiest/second-dummy/second-dummy.component';
import { AnotherDummyComponent } from './the-dummiest/first-dummy/another-dummy/another-dummy.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TheDummiestModule
  ],
  exports: [
    FirstDummyComponent,
    SecondDummyComponent,
    AnotherDummyComponent
  ]
})
export class DummierModule { }
